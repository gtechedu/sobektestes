#!/bin/sh

maquinas(){
    echo 'Limpando containers'
    #para e remove todos os containers
    docker container stop $(docker container ls -aq)
    docker container rm $(docker container ls -aq)

    clear
    echo 'Iniciando mineradores'
    #inicia os mineradores
    docker run -p 5000:5000 -d --name yake liaad/yake-server:latest
    docker run -p 5001:80 -d --name sobek gtechedu/sobek-webservice

    #inicia o banco de dados
    docker volume create pgsobek
    docker volume create pgadmin
    docker run -d --name sobekdb -v pgsobek:/var/lib/postgresql/data -e POSTGRES_DB=sobek -p 5432:5432 -e POSTGRES_USER=sobek -e POSTGRES_PASSWORD=sobek postgres 
    docker pull dpage/pgadmin4
    docker run -d --name pgadmin -p 5050:80 \
        -v pgadmin:/var/lib/pgadmin \
        -e "PGADMIN_DEFAULT_EMAIL=user@eny.com" \
        -e "PGADMIN_DEFAULT_PASSWORD=1234" \
        --link sobekdb:sobekdb dpage/pgadmin4


    #inicia 
    docker run -e "WEBAPP_ROOT=./" -v /tmp/sobekWebService:/tmp/eny -v $PWD:/app -e WEBAPP_USER_ID=$(id -u) -p 80:80 --link sobek --link yake --name testeSOBEK enyalius/fulldev:latest

    eny browsersync testeSOBEK 
}

if [ -f ".ready" ]
then
    echo "Iniciando o projeto SOBEK Validação"
    maquinas
else
    echo "Parece que é nossa primeira execução vamos realizar algumas tarefas."
    echo "--------"
    echo "Se acontecer algum erro por aqui verifique se você tem o git devidamente instalado em seu computador."
    git submodule init
    git submodule update
    echo "Agora que você tem tudo pronto vou passar a bola para o Enyalius. "
    chmod +x ./core/enyalius
    ./core/enyalius gitconfig 
    
    touch .ready 
    maquinas
   
         
fi