## Sobek X Yake ##

Teste do Sobek x Yake as bases de dados estão na pasta `en` e `pt` e os dados raw assim como amostras da mineração estão em `DATA_RESULT`. O resumo e os dados trabalhados você encontra em [result_all.ods](result_all.ods)

* 1º Round: Yake 3-ngram X sobek default
* 2º Round: Sobek + Acronimos, Mais resultados (corte) e sinonimos em PT-br (3x)