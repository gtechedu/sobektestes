<?php
require 'app/include.php';



$WEBSERVICE = 'http://sobek/bigSobek.php'; //Para o ambiente externo deixei a porta 5001 disponível
$YAKE = 'http://yake:5000/yake/';

$DIR = __DIR__ . '/en/citeulike180/';

$MAX = 5;

$start = 0;

$count = 1;

$REQSOBEK = 'args={

}';
echo '<pre>';
$dataFinal = [];
if ($handle = opendir($DIR . '/docs')) {
    while (false !== ($file = readdir($handle))) {
        if ($file != '.' && $file != '..' && $MAX >= $count ) {
            if($count < $start){
                $count ++;
                continue;
            }

            $f = $file;
            $keyFile = rtrim($file, '.txt') . '.key';
            $keyFile = $DIR . 'keys/' . $keyFile;
            $file = $DIR . 'docs/' . $file;
            
            $data = file_get_contents($file);
            $keys = explode(PHP_EOL, file_get_contents($keyFile));

            $sobek = requisitaSobek($data, sizeof($keys));
            $yake = requisitaYake($data, sizeof($keys));
            $dados = [
                'FILE'=> $f,
                'KEY_COUNT' => sizeof($keys),
                'SOBEK_COUNT' => sizeof($sobek),
                'SOBEK_ACERTO' => 0 ,  
                'YAKE_COUNT' => sizeof($yake), 
                'YAKE_ACERTO' => 0               
           
            ];
           
            //remove keys duplicadas

            foreach ($keys as $key) {
                $key = rtrim(strtolower($key), "\r");
                if(in_array($key, $sobek)){
                    $dados['SOBEK_ACERTO']++;
                    $dados['SOBEK_ACERTO_KEY'][] = $key;
                }else{
                    $dados['SOBEK_ERRO_KEY'][] = $key;
                }  
                
                if(in_array($key, $yake)){
                    $dados['YAKE_ACERTO']++;
                    $dados['YAKE_ACERTO_KEY'][] = $key;
                }else{
                    $dados['YAKE_ERRO_KEY'][] = $key;
                }  

            }

            echo '<br> File:' . $file;
            echo '<br> Key File:' . $keyFile;
            echo '<br> Text (part):' . substr($data, 0, 200);
            sort($keys);
            sort($sobek);
            sort($yake);

            echo '<br> Keys:' . json_encode($keys);

            echo '<br> <hr>Sobek:' . json_encode($sobek);
            echo '<br> <hr>Yake:' . json_encode($yake);


            print_r($dados);

            echo '<hr>';

            //$keys = file_get_contents();

            $count++;
            $dataFinal[] = $dados;
        }
    }
} else {
    die('Não consegui abrir o dir' . $DIR);
}
closedir($handle);


echo '</pre>';


$retorno = ob_get_contents();
ob_end_clean();

if(isset($_GET['CSV'])){
    echo '"FILE";"KEY_COUNT";"SOBEK_COUNT";"SOBEK_ACERTO";"YAKE_COUNT";"YAKE_ACERTO" <br>' ;
    foreach($dataFinal as $linha){
        echo '"' . $linha['FILE'] . '";';
        echo '"' . $linha['KEY_COUNT'] . '";';
        echo '"' . $linha['SOBEK_COUNT'] . '";';
        echo '"' . $linha['SOBEK_ACERTO'] . '";';
        echo '"' . $linha['YAKE_COUNT'] . '";';
        echo '"' . $linha['YAKE_ACERTO'] . '"; <br>';
    }
}else{
    echo $retorno;
}
