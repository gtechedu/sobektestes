<?php
error_reporting(669696);
ob_start();


$WEBSERVICE = 'http://sobek/bigSobek.php'; //Para o ambiente externo deixei a porta 5001 disponível
$YAKE = 'http://yake:5000/yake/';

$DIR = __DIR__ . '/en/wiki20/';

$MAX = 100;

$start = 0;

$count = 1;

$REQSOBEK = 'args={

}';
echo '<pre>';
$dataFinal = [];
if ($handle = opendir($DIR . '/docs')) {
    while (false !== ($file = readdir($handle))) {
        if ($file != '.' && $file != '..' && $MAX >= $count ) {
            if($count < $start){
                $count ++;
                continue;
            }

            $f = $file;
            $keyFile = rtrim($file, '.txt') . '.key';
            $keyFile = $DIR . 'keys/' . $keyFile;
            $file = $DIR . 'docs/' . $file;
            
            $data = file_get_contents($file);
            $keys = explode(PHP_EOL, file_get_contents($keyFile));

            $sobek = requisitaSobek($data, sizeof($keys));
            $yake = requisitaYake($data, sizeof($keys));
            $dados = [
                'FILE'=> $f,
                'KEY_COUNT' => sizeof($keys),
                'SOBEK_COUNT' => sizeof($sobek),
                'SOBEK_ACERTO' => 0 ,  
                'YAKE_COUNT' => sizeof($yake), 
                'YAKE_ACERTO' => 0               
           
            ];
           
            //remove keys duplicadas

            foreach ($keys as $key) {
                $key = rtrim(strtolower($key), "\r");
                if(in_array($key, $sobek)){
                    $dados['SOBEK_ACERTO']++;
                    $dados['SOBEK_ACERTO_KEY'][] = $key;
                }else{
                    $dados['SOBEK_ERRO_KEY'][] = $key;
                }  
                
                if(in_array($key, $yake)){
                    $dados['YAKE_ACERTO']++;
                    $dados['YAKE_ACERTO_KEY'][] = $key;
                }else{
                    $dados['YAKE_ERRO_KEY'][] = $key;
                }  

            }

            echo '<br> File:' . $file;
            echo '<br> Key File:' . $keyFile;
            echo '<br> Text (part):' . substr($data, 0, 200);
            sort($keys);
            sort($sobek);
            sort($yake);

            echo '<br> Keys:' . json_encode($keys);

            echo '<br> <hr>Sobek:' . json_encode($sobek);
            echo '<br> <hr>Yake:' . json_encode($yake);


            print_r($dados);

            echo '<hr>';

            //$keys = file_get_contents();

            $count++;
            $dataFinal[] = $dados;
        }
    }
} else {
    die('Não consegui abrir o dir' . $DIR);
}
closedir($handle);


echo '</pre>';


$retorno = ob_get_contents();
ob_end_clean();

if(isset($_GET['CSV'])){
    echo '"FILE";"KEY_COUNT";"SOBEK_COUNT";"SOBEK_ACERTO";"YAKE_COUNT";"YAKE_ACERTO" <br>' ;
    foreach($dataFinal as $linha){
        echo '"' . $linha['FILE'] . '";';
        echo '"' . $linha['KEY_COUNT'] . '";';
        echo '"' . $linha['SOBEK_COUNT'] . '";';
        echo '"' . $linha['SOBEK_ACERTO'] . '";';
        echo '"' . $linha['YAKE_COUNT'] . '";';
        echo '"' . $linha['YAKE_ACERTO'] . '"; <br>';
    }
}else{
    echo $retorno;
}

function requisitaSobek($texto, $nTermos)
{
    global $WEBSERVICE;
    $browser = new BrowserPOST();

    $dados['data'] = $texto;
    $dados['args'] = '{"lang":"EN","return":"TERMS", "nTerms":"' . ($nTermos + 2 ). '"}';
    //$dados['tree'] = '["tagVerbos","tagVebos","tagAdverbio", "tagDeterminante", "tagPronome"]'; //, tagPronome tagPreposicao tagAdje tagCardinal tagInterjeicao

    $browser->setRequestArray($dados);
    $browser->setUrl($WEBSERVICE);

    $var = $browser->getResult();


    $terms = explode(PHP_EOL, $var);

    $data = [];
    foreach ($terms as $term) {
        $data[] = strtolower(explode(',', $term)[0]);
    }
    return $data;
}

function requisitaYake($texto, $nTermos)
{
    global $YAKE;
    $browser = new BrowserPOST();
    $arg = new YakeArg();
    $arg->text = $texto;
    $arg->number_of_keywords = $nTermos;
    $dados = json_encode($arg);
 
    $url = $YAKE;    

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("Content-type: application/json"));
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $dados);

    $json_response = curl_exec($curl);

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    curl_close($curl);

    $response = json_decode($json_response, true);
    $palavras = [];
    foreach($response as $item){
        $palavras[] = strtolower($item['ngram']);
    }

    return $palavras;
}

class YakeArg{
    public $language =  'en';
    public $max_ngram_size = 3;
    public $number_of_keywords = 10;
    public $text;
}

class BrowserPOST
{

    /**
     * @var CurlResourse
     */
    protected $ch;
    protected $options;
    private $requestArgs;
    private $header;
    private $result = null;
    private $body;
    private $cookieFile;
    private $cookies = array();
    protected $url = false;
    private $debug = false;

    /**
     *
     * @param EnderecoCookieFile $cookieFile
     * @throws Exception
     */
    public function __construct($cookieFile = false)
    {
        $this->cookieFile = $cookieFile ? $cookieFile : $this->geraArquivoCookie();

        $this->ch = curl_init();

        if (false === $this->ch) {
            throw new Exception('failed to initialize');
        }
        $this->options();
    }

    /**
     * Realiza a requisição propiamente dita. Caso tenha sido setada
     *
     * @param type $url
     * @throws Exception
     */
    public function requisita($url = false)
    {
        if (!empty($url)) {
            $this->setUrl($url);
        }
        $this->enviaCookies();
        curl_setopt_array($this->ch, $this->options);
        $result = curl_exec($this->ch);
        if (false === $result) {
            throw new \ProgramacaoException(curl_error($this->ch), curl_errno($this->ch));
        }
        $result = utf8_decode($result);
        $headerSize = curl_getinfo($this->ch, CURLINFO_HEADER_SIZE);
        $this->header = substr($result, 0, $headerSize);
        $this->result = substr($result, $headerSize, strlen($result) - $headerSize);
    }

    public function setReferer($referer)
    {
        curl_setopt($this->ch, CURLOPT_REFERER, $referer);
    }

    public function setOption($OPTION, $value)
    {
        curl_setopt($this->ch, $OPTION, $value);
    }

    /**
     * Array chave valor de variaveis do tipo post.
     *
     * @param Array $array
     */
    public function setRequestArray($array)
    {
        $this->requestArgs = $array;
        #FIXME Ver a questão de array http_build_query
        $this->options[CURLOPT_POSTFIELDS] = $array;
    }

    public function setUrl($url)
    {
        if (!($url == filter_var($url, FILTER_SANITIZE_URL))) {
            throw new Exception("URL Inválida" . $url, 1);
        }
        $this->url = $url;
        $this->options[CURLOPT_URL] = $url;
    }

    public function getBodyCode()
    {
        if (is_null($this->result)) {
            $this->requisita();
        }
        return htmlspecialchars($this->result, ENT_QUOTES | ENT_SUBSTITUTE, 'utf-8');
    }

    public function getHeader()
    {
        if (is_null($this->result)) {
            $this->requisita();
        }
        return $this->header;
    }

    /**
     * Retorna uma string com
     * @return String
     */
    public function getResult()
    {
        if (is_null($this->result)) {
            $this->requisita();
        }
        return utf8_encode($this->result);
    }

    public function imprimeHeader($fonte = true)
    {
        if (!$this->header) {
            $this->requisita();
        }
        $result = $this->header;
        if ($fonte) {
            echo '<pre>', $result, '</pre>';
            return;
        }
        echo $result;
    }

    public function imprimeBody($fonte = true)
    {
        if (!$this->result) {
            $this->requisita();
        }
        $result = $this->result;
        if ($fonte) {
            $r = htmlspecialchars($result, ENT_QUOTES | ENT_SUBSTITUTE, 'utf-8');
            echo '<pre>', $r, '</pre>';
            return;
        }
        echo $result;
    }

    public function enviaCookies()
    {
        if (count($this->cookies)) {
            curl_setopt($this->ch, CURLOPT_COOKIE, implode('; ', $this->cookies));
        }
    }

    public function getCookieFile()
    {
        return $this->cookieFile;
    }

    public function setCookieFile($cookieFile)
    {
        $this->cookieFile = $cookieFile;
    }

    public function setCookie($chave, $valor)
    {
        $this->cookies[] = $chave . '=' . $valor;
    }

    private function geraArquivoCookie()
    {
        return '/tmp/cookie' . md5(uniqid()) . '.txt';
    }

    protected function options()
    {
        $headers = array("Content-Type:multipart/form-data",
            'Connection: keep-alive',
        ); // cURL headers for file uploading

        $this->options = array(
            CURLOPT_HEADER => true,
            CURLINFO_HEADER_OUT => 1,
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_VERBOSE => 1,
            CURLOPT_COOKIEFILE => $this->cookieFile,
            CURLOPT_COOKIEJAR => $this->cookieFile,
            CURLOPT_COOKIESESSION => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_USERAGENT => $this->getUserAgent(),
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_UNRESTRICTED_AUTH => 1,
        );
    }

    final protected function getUserAgent()
    {
        return isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0';
    }

    public function __destruct()
    {
        curl_close($this->ch);
    }

    public function ativaDebug()
    {
        curl_setopt($this->ch, CURLOPT_VERBOSE, true);
        $this->debug = fopen('php://stderr', 'w+');
        curl_setopt($this->ch, CURLOPT_STDERR, $this->debug);
    }

    public function logDebug()
    {
        rewind($this->debug);
        return [
            stream_get_contents($this->debug),
            $this->requestArgs,
            http_build_query($this->requestArgs),
            curl_getinfo($this->ch),
        ];
    }

}
