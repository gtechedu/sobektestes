<?php

require '.env.php';
require 'app/Gpt.class.php';
require 'app/metadata.php';


$gpt = new Gpt();

//Para acessar as bases use http://localhost/sobektestes/gpt.php?base=X
$base = isset($_GET['base']) ? $BASE[$_GET['base']]['path'] : '/en/wiki20/'; 

$DIR = __DIR__ . $base;

$MAX = 5;

$start = 0;

echo '<pre>';

list($retorno, $dataFinal) = mineraGPT($DIR, $start, $MAX);

if(isset($_GET['CSV'])){
    echo '"FILE";"KEY_COUNT";"GPT COUNT";"GPT ACERTO"; <br>' ;
    foreach($dataFinal as $linha){
        echo '"' . $linha['FILE'] . '";';
        echo '"' . $linha['KEY_COUNT'] . '";';
        echo '"' . $linha['GPT3_COUNT'] . '";';
        echo '"' . $linha['GPT3_ACERTO'] . '"; <br>';
    }
}else{
    echo $retorno;
}
echo '</pre>';


function requisitaGPT($data, $numeroKeys){
    $gpt = new Gpt();

    $pergunta ='Quais somente os ' . $numeroKeys . ' termos mais relevantes para o texto separado por virgulas';

    $resultado = $gpt->makeRequest($data, $pergunta); #, 'gpt-4o');

    $rOBJ = json_decode($resultado);

    //var_dump($rOBJ->choices[0]->message->content);

    return explode(', ', $rOBJ->choices[0]->message->content); 
}

function mineraGPT($DIR, $start, $MAX)
{

    $count = 1;

    $dataFinal = [];
    if ($handle = opendir($DIR . '/docs')) {
        while (false !== ($file = readdir($handle))) {
            if ($file != '.' && $file != '..' && $MAX >= $count) {
                if ($count < $start) {
                    $count++;
                    continue;
                }

                $f = $file;
                $keyFile = rtrim($file, '.txt') . '.key';
                $keyFile = $DIR . 'keys/' . $keyFile;
                $file = $DIR . 'docs/' . $file;

                $data = file_get_contents($file);

                $keys = trim(file_get_contents($keyFile));
                $keys = str_replace(' and ', PHP_EOL, $keys); //Quebra termos compostos
                $keys = preg_replace('/[\r\t]/', ' ', $keys); //Remove tabs  
                $keys = preg_replace('/\s(?=\s)/', '', $keys); //Remove espaços duplos
                $keys = str_replace('-', ' ', $keys); //Remove espaços duplos
                $keys = preg_replace("/\([^)]+\)/", '', $keys); //Remove contextos
                $keys = strtolower($keys);


                $keys = explode(PHP_EOL, $keys);
                $keys = array_unique($keys);

                $gpt = requisitaGPT($data, sizeof($keys));
                $dados = [
                    'FILE' => $f,
                    'KEY_COUNT' => sizeof($keys),
                    'GPT3_COUNT' => sizeof($gpt),
                    'GPT3_ACERTO' => 0,
                ];

                //remove keys duplicadas
                //conta Keys
                foreach ($keys as $key) {
                    $key = rtrim(strtolower($key), "\r");
                    if (in_array($key, $gpt)) {
                        $dados['GPT3_ACERTO']++;
                        $dados['GPT3_ACERTO_KEY'][] = $key;
                    } else {
                        $dados['GPT3_ERRO_KEY'][] = $key;
                    }

                }

                $count++;

                $dataFinal[] = $dados;
                if (isset($_GET['CSV'])) {
                    continue;
                }

                echo '<br> File:' . $file;
                echo '<br> Key File:' . $keyFile;
                echo '<br> Text (part):' . substr($data, 0, 200);
                sort($keys);
                sort($gpt);

                echo '<br> Keys:' . json_encode($keys);

                echo '<br> <hr>GPT:' . json_encode($gpt);

                print_r($dados);

                echo '<hr>';
            }
        }
    } else {
        die('Não consegui abrir o dir' . $DIR);
    }
    closedir($handle);

    echo '</pre>';

    $retorno = ob_get_contents();
    ob_end_clean();

    return [$retorno, $dataFinal];
}




function processaArquivos($arquivos, $base){
    $data = [];
    foreach ($arquivos as $arquivo){
        mineraArquivo($arquivo, $base, $data);
    }
    return ['', $data];
}