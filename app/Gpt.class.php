<?php

define ('CACHE', __DIR__ .'/../z_data/');

class Gpt
{
    private $CLIENT_API_KEY;
    private $OPENAI_API_KEY = GPT_API_KEY;
    private $url = 'https://api.openai.com/v1/chat/completions';
    private $cacheDir = CACHE .'gpt';

    public function __construct($CLIENT_API_KEY = null)
    {
        if(isset($CLIENT_API_KEY)){
            $this->OPENAI_API_KEY = $CLIENT_API_KEY;
        }

    }

    //Estudar de colocar isso em uma trait para facilitar o uso em outras partes do sistema.
    private function cacheVerify($key){
        if(!file_exists($this->cacheDir)){
            mkdir($this->cacheDir, 0755, true);
        }
        $key = md5($key);
        $file = $this->cacheDir . '/' . $key;
        if(file_exists($file)){
            return file_get_contents($file);
        }
        return false;
    }

    private function makeCache($key, $value){
        $key = md5($key);
        file_put_contents($this->cacheDir . '/' .$key, $value);
    }

    public function makeRequest($userContent, $systemContent, $model = "gpt-3.5-turbo"){

        $cache = $this->cacheVerify($userContent . '-' . $systemContent. ' - ' . $model);

        if($cache){
            return $cache;
        }
        
        $data = array(
            'model' => $model,
            'messages' => array(
                array(
                    'role' => 'system',
                    'content' => $systemContent
                ),
                array(
                    'role' => 'user',
                    'content' => $userContent
                )
            )
        );
        $textAnalisys = $this->textAnalisys($data);

        $this->makeCache($userContent . '-' . $systemContent . ' - ' . $model, $textAnalisys);
        return $textAnalisys;
        
    }
    
    private function textAnalisys($data){
        try{

            $ch = curl_init($this->url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: Bearer ' . $this->OPENAI_API_KEY
            ));
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            
            
            $response = curl_exec($ch);
            
            if ($response === false) {
                return false;
            } else {
                return $response;
            }
    
      
        }catch(Exception $e){
            $e->getMessage();
        }finally{
            curl_close($ch);
        }

            
    }
}
