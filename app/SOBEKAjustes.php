<?php

function show($var){
    echo '<h1> Achei: ' . $var .  '</h1>';
}

function trataParteAcronimo($acron){
    $tratado = str_replace('problem', '', $acron);
    $tratado = str_replace('techniques', '', $tratado);
    $tratado = str_replace('technique', '', $tratado);
    $tratado = str_replace('-', ' ', $tratado);
    return trim($tratado);
}

function getTitulo($data, $title)
{
    if (!$title) {
        return '';
    }
    $length = strpos($data, PHP_EOL); // Extrai titulo
    echo 'duplicando titulo';
    return substr($data, 0, $length);
}

function adicionaAcronimos($keys, $file)
{
    print_r($keys);
    $exec = "node ./app/acronimo.js " . $file;
    // echo $exec;
    exec($exec, $output, $returnvar);

    if ($returnvar == 0) {

        // Isso parece lisp - Resumindo:
        // 1- Concatena retorno do exec; 2 - deixa tudo em minusculo; 
        // 3 - transforma aspas simples em duplas; 4 - Transforma em array PHP
        $acronimos = json_decode(str_replace("'", '"', strtolower(implode('', $output))));
        var_dump($acronimos);
        foreach ($acronimos as $acron) {
            //caso 1 a sigla está nas keys
            if (in_array($acron[0], $keys)) {
                $keys[] = $acron[1];
                continue;
            }
            //caso 2 parte da sigla é um termo importante 
            foreach($keys as $key){
                $terms = explode(' ', $acron[1]);
                if(in_array($key, $terms)){
                    show($acron[1]); 
                    $keys[] = $acron[1];//adiciona completo
                    $trat = trataParteAcronimo($acron[1]);
                    if($trat != $acron[1]){
                        $keys[] = $trat;//adiciona tratado comuns (isso aqui pode ser melhorado)   
                    }
                }
            }
        }
    } else {
        echo '<erro>Problema na execução: COD ' . $returnvar . '</erro>';
        echo '<message>' . $output . '</message>';
    }
    print_r($keys);
    return $keys;
}

function removeSobras($keys, $limite)
{

    $return = $keys2 = [];

    //remove ing e ed. =>verbos
    foreach ($keys as $key) {
        if (substr($key[0], -2) == 'ed') {
            continue;
        }
        $keys2[] = $key;
    }

    //Trabalha mesmo melhorar isso pois pode ter vários empates (passar para o Leonardo).
    for ($i = 0; $i < ($limite - 1) && $i < (sizeof($keys) - 1) && $i < (sizeof($keys2) - 1); $i++) {
        $return[] = $keys2[$i][0];
    }

    if (sizeof($keys) < $limite || sizeof($keys2) < $limite) { //Retornou menos keys que o limite
        return $return;
    }

    if (sizeof($keys2) == $limite || $keys2[$limite - 1][1] > $keys2[$limite][1]) { //As chaves não estão empatadas
        $return[] = $keys2[$limite - 1][0];
    } else {
        //Consulta no google mas nesse caso vai o segundo hehehe
        $return[] = $keys2[$limite][0];

    }
    //print_r($keys);
    //print_r($return);
    return $return;
}

function getKeys($DIR, $file)
{
    $keyFile = rtrim($file, '.txt') . '.key';
    $keyFile = $DIR . 'keys/' . $keyFile;
    echo $keyFile;
    $keys = trim(file_get_contents($keyFile));
    $keys = str_replace(',', PHP_EOL, $keys); //remove contextos

    $keys = str_replace(' and ', PHP_EOL, $keys); //Quebra termos compostos
    $keys = preg_replace('/[\r\t]/', ' ', $keys); //Remove tabs
    $keys = preg_replace('/\s(?=\s)/', '', $keys); //Remove espaços duplos
    $keys = str_replace('-', ' ', $keys); //Remove hifens para condizer com os mineradores
    $keys = preg_replace("/\([^)]+\)/", '', $keys); //Remove contextos

    $keys = explode(PHP_EOL, $keys);
    $keys = array_unique($keys);

    return $keys;
}

function sobekV2($DIR, $start, $MAX, $title = false, $acronimos = false)
{

    $count = 1;
    $REQSOBEK = 'args={

    }';

    $dataFinal = [];
    if ($handle = opendir($DIR . '/docs')) {
        while (false !== ($file = readdir($handle))) {
            if ($file != '.' && $file != '..' && $MAX >= $count) {
                if ($count < $start) {
                    $count++;
                    continue;
                }

                $keys = getKeys($DIR, $file);

                $path = $DIR . 'docs/' . $file;

                $data = file_get_contents($path);

                $titulo = getTitulo($data, $title);
                // $data = removeSiglas($data, $path);
                $acronimos = $acronimos ? $path : false;

                list($sobek, $tam) = requisitaSobekV2($data . PHP_EOL . $titulo, sizeof($keys), $acronimos);
                $dados = [
                    'FILE' => $file,
                    'KEY_COUNT' => sizeof($keys),
                    'SOBEK_COUNT' => $tam,
                    'SOBEK_ACERTO' => 0,
                ];

                //remove keys duplicadas
                //conta Keys

                foreach ($keys as $key) {
                    $key = rtrim(strtolower($key), "\r");
                    if (in_array($key, $sobek) || in_array(rtrim($key, 's'), $sobek)) {
                        $dados['SOBEK_ACERTO']++;
                        $dados['SOBEK_ACERTO_KEY'][] = $key;
                    } else {
                        $dados['SOBEK_ERRO_KEY'][] = $key;
                    }

                }

                $count++;

                $dataFinal[] = $dados;
                if (isset($_GET['CSV'])) {
                    continue;
                }

                echo '<br> File:' . $file;
                echo '<br> Text (part):' . substr($data, 0, 200);
                sort($keys);
                sort($sobek);

                echo '<br> Keys:' . json_encode($keys);
                echo '<br> <hr>Sobek:' . json_encode($sobek);

                print_r($dados);

                echo '<hr>';

            }
        }
    } else {
        die('Não consegui abrir o dir' . $DIR);
    }
    closedir($handle);

    echo '</pre>';

    $retorno = ob_get_contents();
    ob_end_clean();

    return [$retorno, $dataFinal];
}

function requisitaSobekV2($texto, $nTermos, $acronimos = false)
{
    global $WEBSERVICE;
    $browser = new BrowserPOST();

    $dados['data'] = $texto;

    $dados['args'] = '{"lang":"EN","return":"TERMS", "nTerms":' . ($nTermos * 2) . ' }';
    $dados['tree'] = '["tagVerbos","tagVebos","tagAdverbio", "tagDeterminante", "tagPronome"]'; //, tagPronome tagPreposicao tagAdje tagCardinal tagInterjeicao

    $browser->setRequestArray($dados);
    $browser->setUrl($WEBSERVICE);

    $var = $browser->getResult();

    $terms = explode(PHP_EOL, $var);

    $data = [];
    foreach ($terms as $term) {
        $piece = explode(',', $term);
        $data[] = [strtolower($piece[0]), $piece[1]];
    }

    $chaves = removeSobras($data, $nTermos);
    $tam  = sizeof($chaves);
    if ($acronimos) {
        $chaves = adicionaAcronimos($chaves, $acronimos);
    }

    return [$chaves, $tam];
}
