const fs = require('fs')


fs.readFile(process.argv[2], 'utf8', (err, data) => {
    if (err) {
        console.error(err)
        return
    }
    getText(data);
});


function getText(data) {

    var text = data.replace(/-/g, "- ").split(/[\s]+/);
    //console.log(text)



    var termo;
    var matriz = [];
    var i = 0;
    text.forEach((word, index) => {
        //console.log("palavra " + word);
        if (/(\({1}[A-Z]{2,})\){1}/.test(word)) { //Se for acronimo
            //if((/(\({1}|\,{1})[A-Z]{1,}[a-z]{0,}(\){1}|\,{1})/).test(word)){
            //if((/[A-Z]{1,}[a-z]{0,}(\){1}|\,{1})/).test(word)){
            //console.log("entrou " + word+" "+ index);
            termo = findTermo(text, index, word.match(/[A-Za-z]+/));

            if (termo) {
                termo = termo.replace(/- /g, "-");
                matriz[i++] = [word.match(/[A-Za-z]+/)[0], termo];
            }
        }
    });
    console.log(matriz)
        //console.log(JSON.stringify(matriz));
}

function findTermo(text, index, word) {
    word = String(word).toUpperCase();
    //console.log(word)
    var tam = word.length;
    var isAcronimo = true;
    var firstIndex;
    var encontrado = new Array(tam).fill(false);

    //console.log("index"+ index + " word "+ word);//posição do acronimo no texto e acronimo
    //console.log("teste " + text[index-1] + " " + (text[index-1]).charAt(0) + " word "+ word.charAt(tam-1) ); 
    firstIndex = index - 1;

    for (var y = tam - 1; y >= 0 && isAcronimo && !encontrado[0]; y--) { //percorre acronimo
        for (var x = firstIndex; x >= index - (3 * tam) && !encontrado[y]; x--) { //percorre texto em 3 vezes o tamanho do acronimo
            //console.log("texto " + (text[x])+ " "+ word.charAt(y));  
            if (typeof text[x] !== "undefined") { //provisorio
                if ((text[x]).charAt(0).toUpperCase() === word.charAt(y)) {
                    //console.log("true");
                    encontrado[y] = true;
                    if (x < firstIndex) {
                        firstIndex = x;
                    }
                } else {
                    //console.log("false");                        
                }
            }

        }
        if (encontrado[y] != true) {
            isAcronimo = false;
        }
    }

    if (isAcronimo) {
        text = text.slice(firstIndex, index);
        return text.join(" ");
    } else {
        return false;
    }


}