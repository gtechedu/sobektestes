<?php
$BASE = [
    1 => ['name'=>'Pubmed', 'path'=> '/en/PubMed/'],
    2 => ['name'=>'CiteuLike', 'path'=> '/en/citeulike180/'],
    3 => ['name'=>'Krapivin', 'path'=> '/en/Krapivin2009/'],
    4 => ['name'=>'SemEval2017', 'path'=> '/en/SemEval2017/'],
    5 => ['name'=>'SemEval2010', 'path'=> '/en/SemEval2010/'],
    6 => ['name'=>'500N', 'path'=> '/en/500N-KPCrowd/'],
    7 => ['name'=>'Fao30', 'path'=> '/en/SemEval2017/'],
    8 => ['name'=>'Fao780', 'path'=> '/en/SemEval2017/'],
    9 => ['name'=>'Inspec', 'path'=> '/en/SemEval2017/'],
    10 => ['name'=>'Wiki', 'path'=> '/en/SemEval2017/'],
    11 => ['name'=>'Nguyen', 'path'=> '/en/SemEval2017/'],
    12 => ['name'=>'Schutz', 'path'=> '/en/SemEval2017/'],
    13 => ['name'=>'Theses', 'path'=> '/en/theses100/'],
    14 => ['name'=>'110-ptn', 'path'=> '/pt/'],
    15 => ['name'=>'www*', 'path'=> '/en/theses100/'],
    16 => ['name'=>'kdd*', 'path'=> '/en/theses100/'],
];

